# OnLoki

OnLoki is a quick way to get a subdomain for your Lokinet SNApp for free. You can check it out at http://on.loki  
Here is its source code!

## Running

```bash
sudo apt install npm
npm install
cp prisma/schema.db prisma/dev.db
npm run generate
```

If you want to run a test server, which will help you to spot errors and debug, and will have auto-refresh:

```bash
npm run dev
```

If you want to run a production server, which will load faster:

```bash
npm run build # You only need to do this when you change something and on first run
npm run prod
```

You can even set up a SystemD service that auto-restarts the production server. First edit the `onloki.service` file replacing "YOUR_USER" with the username of your account on the host machine (use `whoami` if you don't know that for some reason), and "ONLOKI_PATH" with the absolute path of the folder where you cloned this repository. Then:

```bash
sudo cp onloki.service /etc/systemd/system/onloki.service
sudo systemctl start onloki
sudo systemctl enable onloki
```

## Support

You can support this project by sending some OXEN. Thank you very much!

```
L7n8qURqkbSPraTADG8J6ujE8acs9crvZVzXTUacShWVSjLUEWYXCUTKuBvGTDuffDdNCR1iuGPT5RSoAyNgoBZuQov15fG
```

## License

All the files in this repository are licensed under the GNU GPL 3.0 or later.

```
OnLoki - Free "domains" for Lokinet SNApps.
Copyright (C) 2022 MassiveBox and OnLoki contributors.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

