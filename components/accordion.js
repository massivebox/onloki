import React, { useState } from 'react';

const Accordion = ({ title, content }) => {

  const [isActive, setIsActive] = useState(false);

  return (
    <div className="accordion-item">
      <div className="accordion-title" onClick={() => setIsActive(!isActive)}>
        <p>
          <b>{title}</b>
          <span style={{float: "right"}}><b>{isActive ? '-' : '+'}</b></span>
        </p>
      </div>
      {isActive && <div className="accordion-content"><hr /><p dangerouslySetInnerHTML={{__html: content}}></p></div>}
    </div>
  );
};

export default Accordion;