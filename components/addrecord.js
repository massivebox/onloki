import React, { Component } from 'react';

class AddRecordForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
        status : "",
        message: "",
        subdomain: "",
        pointsto: "",
    }
  }

  render() {

    return ( <div>
      <form onSubmit={async (e) => {
          e.preventDefault();
          if (this.state.subdomain.length === 0 || this.state.pointsto.length === 0) {
            this.setState({ status: "error", message: "You have some blank fields!" });
          }else{
            
            let resp, data;
            try {
              resp = await fetch(
                `http://on.loki/api/addrecord`,
                {
                  method: "POST",
                  credentials: "include",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({ subdomain: this.state.subdomain, pointsto: this.state.pointsto }),
                }
              );
              data = await resp.json();
            } catch (error) {
              console.log(error);
              this.setState({ status: "error", message: "Internal error" });
              return;
            }

            if(data.ok) {
              this.setState({ status: "success", message: `<b>Success!</b> Your domain is now registred.<br />
              Ping this URL at least once a week to keep your domain active: <code>http://on.loki/api/renew?uuid=${data.uuid}</code><br />
              Ping this URL to delete your domain: <code>http://on.loki/api/delete?uuid=${data.uuid}</code>` });
            }else{
              this.setState({ status: "error", message: data.error });
            }

          }
        }}
        >
        <div>

          <input
            value={this.state.subdomain}
            onChange={(e) => this.setState({subdomain: e.target.value})}
            type="name"
            placeholder="Name of your choice"
          />.on.loki
          <br />

          <input
            value={this.state.pointsto}
            onChange={(e) => this.setState({pointsto: e.target.value})}
            type="name"
            placeholder="SNApp's current address"
          />.loki
          <br />

          <button type="submit">
            Submit
          </button>

        </div>

      </form>
      
      <div className={ this.state.status } dangerouslySetInnerHTML={{__html: this.state.message}}>
      </div>

    </div>)
  }

}

export default AddRecordForm;