import { PrismaClient } from "@prisma/client";
import { v4 as uuidv4 } from 'uuid';
const prisma = new PrismaClient();

export default async(req, res) => {

  const { subdomain, pointsto } = req.body;

  if(subdomain == "" || pointsto == "") {
    res.json({ ok: false, error: "Missing fields." });
    return;
  }
  if(subdomain.length < 3) {
    res.json({ ok: false, error: "Subdomain must be at least 3 characters long." });
    return;
  }
  if(pointsto.length < 53) {
    res.json({ ok: false, error: "PointsTo doesn't look like a valid SNApp address: it must have at least 53 characters." });
    return;
  }

  var uuid = uuidv4();
  var renew = Math.floor(Date.now() / 1000);

  try {
    await prisma.records.create({
      data: {
        subdomain,
        pointsto,
        uuid,
        renew,
      },
    });
  } catch (error) {
    res.json({ ok: false, error: "This subdomain already exists, or a subdomain already points to this SNApp's address, or internal error." });
    console.log(error);
    return;
  }

  res.status(200).json({ ok: true, uuid: uuid })

}
