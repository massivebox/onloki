import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async(req, res) => {

  const { uuid } = req.query;

  try {
    await prisma.records.delete({
      where: {
        uuid
      },
    });
  } catch (error) {
    res.json({ ok: false, error: "UUID not found or internal error." });
    console.log(error);
    return;
  }

  res.status(200).json({ ok: true })

}
