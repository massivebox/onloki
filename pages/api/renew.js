import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async(req, res) => {

  const { uuid } = req.query;
  var renew = Math.floor(Date.now() / 1000);

  try {
    await prisma.records.update({
      where: {
        uuid
      },
      data: {
        renew
      },
    });
  } catch (error) {
    res.json({ ok: false, error: "UUID not found or internal error." });
    console.log(error);
    return;
  }

  res.status(200).json({ ok: true })

}
