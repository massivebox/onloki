import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async(req, res) => {

  const { subdomain } = req.body;
  if(subdomain == "") {
    res.json({ ok: false, error: "Subdomain not provided" });
    return
  }
  var currentTime = Math.floor(Date.now() / 1000);

  let record;
  try {
    record = await prisma.records.findUnique({
      where: {
        subdomain
      },
    });
  } catch (error) {
    res.json({ ok: false, error: "Subdomain not found or internal error." });
    console.log(error);
    return;
  }

  if(record == null) {
    res.json({ ok: false, error: "Subdomain not found." });
    return;
  }

  if(record.renew < currentTime - 604800) {
    if(record.renew < currentTime - 1209600) {
      try {
        await prisma.records.delete({
          where: {
            subdomain
          },
        });
      } catch (error) {
        res.json({ ok: false, error: "Domain expired, but failed to delete it." });
        console.log(error);
      }
      res.json({ ok: false, error: "Domain expired!" });
      return;
    }    
    res.json({ ok: false, error: "Domain expired! Renew it within a week or it will get deleted." });
    return;
  }

  res.status(200).json({ ok: true, address: record.pointsto })

}
