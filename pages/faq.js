import Head from 'next/head'
import Link from 'next/link'
import Accordion from '../components/accordion'

const accordionData = [
  {
    title: "Is OnLoki a substitute to ONS (Oxen Name Service)?",
    content: `<b>Absolutely not!</b> This is a much inferior product.<br>
    OnLoki is centralized, while ONS is decentralized.<br>
    OnLoki runs on a single server with no uptime guarantee, and ONS runs on more than 1700 servers all around the world.<br>
    OnLoki blocks content as described in the TOS, ONS does not.<br>
    You need to renew your OnLoki address every week, ONS names can be renewed once every 10 years.<br>
    Sounds cool? You can get an ONS name from the <a target="_blank" href="https://docs.oxen.io/downloads" rel="noopener noreferrer">Desktop wallet. (CLEARNET, UNDER CLOUDFLARE)</a>`,
  },
  {
    title: "Who is OnLoki for?",
    content: `OnLoki is for everyone who wants to have a simple, rememberable name for their Lokinet SNApps that respect our TOS, but don't have OXEN to buy a real ONS name.<br>
    If you have enough OXEN, please consider buying an ONS name to support the network.<br>
    If you don't have any OXEN, you can get yourself some on <a target="_blank" href="https://coingecko.com" rel="noopener noreferrer">CoinGecko (CLEARNET, UNDER CLOUDFLARE)</a>.`
  },
  {
    title: "How does OnLoki work?",
    content: `OnLoki works by reverse-proxying all incoming requests from <code>yoursubdomain.on.loki</code> to <code>yourserver.loki</code>.<br>
    Proxying is achieved using the <code>http-proxy</code> library for NodeJS.<br>
    Yes. You read that right. NodeJS. Because I could.`
  },
  {
    title: "Are OnLoki subdomains free? How do I renew my OnLoki subdomain?",
    content: `Yes, OnLoki is free for everyone, and there are no advantages for users who donate.<br>
    However, to prevent spam, you must renew your subdomain at least once a week. If you don't renew your subdomain for more than a week, it will get suspended:
    users won't be able to access your subdomain, but no one will be able to claim it as theirs. After two weeks, the subdomain will be deregistred, and anyone will be able to claim it.<br>
    When you register an OnLoki subdomain, you will be given an URL for renewal. There is no captcha required or anything, so you can simply use a cronjob to curl the URL every 7 days.`
  },
  {
    title: "How can I support OnLoki?",
    content: `That's very kind of you! I try to keep costs low, but at the bare minimum I need 7 OXEN a year for the <code>on.loki</code> domain.<br />
    If you have some OXEN and you're willing to help me pay the fees, feel free to donate any amount of Oxen at this address:<br />
    <code>L7n8qURqkbSPraTADG8J6ujE8acs9crvZVzXTUacShWVSjLUEWYXCUTKuBvGTDuffDdNCR1iuGPT5RSoAyNgoBZuQov15fG</code><br />
    You can also help by contributing to our <a target="_blank" href="http://8bb19w1gugu7yq3cyck63gbo18udodab1b6zr1uykdphm37ushco.loki/massivebox/onloki" rel="noopener noreferrer">development.</a>`
  },
];

const Faq = () => {

  return (
    <div className="container">
      <Head>
        <title>OnLoki</title>
        <meta name="description" content="Get a free subdomain for Lokinet SNApps." />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">

        <h1 className="title">
          On.Loki
        </h1>

        <p className="description">
          Get a short <b>free</b> subdomain to access your Lokinet SNApp.<br />
          <Link href="/">Home</Link> - <Link href="/faq">FAQ</Link> - <Link href="/tos">TOS</Link> - <a target="_blank" href="http://gitea.on.loki/massivebox/onloki" rel="noopener noreferrer">Code</a>
        </p>

        <h1>FAQ</h1>
        <p>Here are some FAQs for OnLoki. If you can't find the answer to your question, contact <code>massivebox</code> (ONS name on Session).<br /></p>

        <div className="accordion" style={{width: "100%"}}>
          {accordionData.map(({ title, content }) => (
            <Accordion title={title} content={content} key={title} />
          ))}
        </div>

      </main>
    </div>
  );

}

export default Faq;