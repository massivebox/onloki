import Head from 'next/head'
import Link from 'next/link'
import AddRecordForm from '../components/addrecord'

export default function Home() {

  return (
    <div className="container">
      <Head>
        <title>OnLoki</title>
        <meta name="description" content="Get a free subdomain for Lokinet SNApps." />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">

        <h1 className="title">
          On.Loki
        </h1>

        <p className="description">
          Get a short <b>free</b> subdomain to access your Lokinet SNApp.<br />
          <Link href="/">Home</Link> - <Link href="/faq">FAQ</Link> - <Link href="/tos">TOS</Link> - <a target="_blank" href="http://gitea.on.loki/massivebox/onloki" rel="noopener noreferrer">Code</a>
        </p>

        <AddRecordForm />

      </main>
    </div>
  )
}