import Head from 'next/head'
import Link from 'next/link'

const Faq = () => {

  return (
    <div className="container">
      <Head>
        <title>OnLoki</title>
        <meta name="description" content="Get a free subdomain for Lokinet SNApps." />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">

        <h1 className="title">
          On.Loki
        </h1>

        <p className="description">
          Get a short <b>free</b> subdomain to access your Lokinet SNApp.<br />
          <Link href="/">Home</Link> - <Link href="/faq">FAQ</Link> - <Link href="/tos">TOS</Link> - <a target="_blank" href="http://gitea.on.loki/massivebox/onloki" rel="noopener noreferrer">Code</a>
        </p>

        <h1>TOS</h1>
        <p>
          This page explains the Terms and Conditions under which the OnLoki service is provided.<br />
          I refuse to provide a definition for all the terms or speak lawyerese. If you don't know what a term means, look it up.
        </p>

        <h2>Conditions to register a subdomain</h2>
        <p>
          You can only register a subdomain if no other subdomain already registred points to the SNApp's address you provided.<br />
          Obviously, you can register a subdomain only if nobody has already registred one with the same name.<br />
          Subdomains are served on a first-come-first-serve basis. If a subdomain expires, it can be registred again.
        </p>

        <h2>Contents limitation</h2>
        <p>
          Here at OnLoki, we're very serious about not wanting to go to jail.<br />
          If your SNApp hosts any kind of content that is illegal under Italian laws, your subdomain will be deleted without warning.<br />
          Additionally, you can't have subdomains that point to SNApps that contain: pornography or soft porn, items on sale (both physical and digital), piracy.<br />
          If you found a OnLoki subdomain that violates these rules, contact me immediately at <code>massivebox</code> on Session.
        </p>

        <h2>General rules</h2>
        <p>
          I can delete any subdomain at any time without any warning if I decide to.<br />
          All the information you provide could be held against you in case of an investigation by the authorities.<br />
          I might update this page at any point in time without warning, and apply the changes retro-actively.
        </p>

      </main>
    </div>
  );

}

export default Faq;