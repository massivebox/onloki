const next = require('next')
var express = require('express')
var httpProxy = require('http-proxy');

const dev = process.env.NODE_ENV == 'development'
const port = process.env.PORT || 3000
const app = next({ dev })
const handle = app.getRequestHandler()
var proxy = httpProxy.createProxyServer({});

app.prepare().then(() => {

  express()
    .use(async function(req, res) {

      var subdomain = req.headers.host;

      if(subdomain == `localhost:${port}` || subdomain == "on.loki") {
        handle(req, res);
        return;
      }

      let resp, data;
      try {
        resp = await fetch(
          `http://on.loki/api/resolve`,
          {
            method: "POST",
            credentials: "include",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ subdomain: subdomain.replace(".on.loki", "").replace(`:${port}`, "") }),
          }
        );
        data = await resp.json();
      } catch (error) {
        res.send("Internal error");
        console.log(error);
        return;
      }

      if(data.ok) {
        proxy.web(req, res, { target: "http://" + data.address + ".loki" });
      }else{
        res.send(data.error);
      }

    })
    .listen(port, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })


}).catch(err => {
    console.log('Error:::::', err)
})